(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(c-report-syntactic-errors t)
 '(c-strict-syntax-p nil))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
(setq default-tab-width 8)
(setq tab-width 8)
(setq tab-stop-list ())
;;设置C语言风格
(add-hook 'c-mode-hook 'linux-c-mode)
(setq imenu-sort-function 'imenu--sort-by-name)
(defun linux-c-mode()
  (define-key c-mode-map [return] 'newline-and-indent)
  (interactive)
  (c-set-style "K&R")
  (c-toggle-hungry-state)
  (setq c-basic-offset 8)
  (imenu-add-menubar-index)
  (which-function-mode)
  )
(setq-default transient-mark-mod t)
(transient-mark-mode t)
(show-paren-mode t)
(global-linum-mode 1) ; always show line numbers
(desktop-save-mode 1)
(setq display-time-day-and-date t)
(setq display-time-24hr-format t)
(setq display-time-format "%Y-%m-%d %H:%M:%S")
(display-time)
(setq column-number-mode t)
(mouse-avoidance-mode 'animate)
(setq mouse-yank-at-point t)
(setq scroll-margin 3
scroll-conservatively 10000)
(setq default-buffer-file-coding-system 'utf-8)
(auto-image-file-mode)
(setq iswitchb-buffer-ignore '("^ " "*Buffer"))
(setq iswitchb-buffer-ignore '("^\\*"))
(setq iswitchb-default-method 'samewindow)
(tabbar-mode 1)
