include "/etc/bind/acl.telcom";
include "/etc/bind/acl.edu"; 

acl "shandong_zibo_chinamobile" {
	218.201.124.0/24;
// telcom heilongjiang
	123.164.0.0/14;
	//220.113.6.0/24;
	211.137.191.0/24;
};

acl "zz"  {
	218.61.76.162/32;
	8.8.8.8/32;
};

acl "local_slave" {
	192.168.2.0/24;
};
view "view_telcom" {
	match-clients {
		//key slaves-dns2-telcom-tsigkey; local_slave;TELCOM; EDU;
		key slaves-dns2-telcom-tsigkey; local_slave; zz;
	};

	allow-transfer { key slaves-dns2-telcom-tsigkey; }; 
	server 192.168.2.46  { keys slaves-dns2-telcom-tsigkey; };

	zone "tongyi.com" {
                type master;
                file "/etc/bind/db.telcom.tongyi.com"; 
                allow-transfer { 192.168.2.46;};
                notify yes;
                also-notify{ 192.168.2.46;};
        };

	zone "tongyi.me" {
		type master;
		file "/etc/bind/db.telcom.tongyi.me";
	};
};
view "view_any" {
	match-clients { any; };
	zone "tongyi.com" {
		type master;
		file "/etc/bind/db.tongyi.com"; 
		allow-transfer { 192.168.2.46;};
		notify yes;
		also-notify{ 192.168.2.46;};
	};

	zone "tongyi.me" {
		type master;
		file "/etc/bind/db.tongyi.me";
		allow-transfer { 192.168.2.46;};
		notify yes;
		also-notify{ 192.168.2.46;};
	};

	zone "t1edu.com" {
		type master;
		file "/etc/bind/db.t1edu.com"; 
		allow-transfer { 192.168.2.46;};
		notify yes;
		also-notify{ 192.168.2.46;};
	};

	// prime the server with knowledge of the root servers
	zone "." {
		type hint;
		file "/etc/bind/db.root";
	};

	// be authoritative for the localhost forward and reverse zones, and for
	// broadcast zones as per RFC 1912

	zone "localhost" {
		type master;
		file "/etc/bind/db.local";
	};

	zone "127.in-addr.arpa" {
		type master;
		file "/etc/bind/db.127";
	};

	zone "0.in-addr.arpa" {
		type master;
		file "/etc/bind/db.0";
	};

	zone "255.in-addr.arpa" {
		type master;
		file "/etc/bind/db.255";
	};

	zone "76.61.218.in-addr.arpa" in{
		type master;
		file "/etc/bind/db.218.61.76.rev";
		allow-transfer { 192.168.2.46;};
		notify yes;
		also-notify{ 192.168.2.46;};
	};
};
