$TTL 3600   	;  1 hour
@            IN SOA  ns1.tongyi.com. admin.tongyi.com. (
                                2011051310        ; serial
                                10800      ; refresh (3 hours)
                                900        ; retry (15 minutes)
                                604800     ; expire (1 week)
                                3600       ; minimum (1 hour)
                                )
$TTL 	2M
@       IN      NS      ns1.tongyi.com.
@       IN      NS      ns2.tongyi.com.

@       IN      MX      5	mx1.t1edu.com.

ns1	IN	A	61.135.205.153
ns2	IN	A	61.135.205.150

mx1	IN	A	218.61.76.163

@       IN      A       218.61.76.163

mail    IN      A       218.61.76.163
smtp    IN      A       218.61.76.163
pop     IN      A	218.61.76.163
imap	IN	A	218.61.76.163
www	IN	A	218.61.76.163

t1edu.com. 	IN      TXT     "v=spf1 ip4:218.61.76.162 a mx mx:mail.t1edu.com ~all"
