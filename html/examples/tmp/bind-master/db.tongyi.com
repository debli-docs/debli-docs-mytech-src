$TTL 3600   	;  1 hour
@            IN SOA  ns1.tongyi.com. admin.tongyi.com. (
                                2011071101         ; serial
                                10800      ; refresh (3 hours)
                                900        ; retry (15 minutes)
                                604800     ; expire (1 week)
                                3600       ; minimum (1 hour)
                                )
;@      IN      CNAME tongyi.ccgslb.net.

@                       IN      NS      ns1.tongyi.com.
@                       IN      NS      ns2.tongyi.com.

@                       IN      MX      0               mx.xinnetvip.com.

mailadmin               IN      CNAME           admin.xinnetvip.com.
mail                    IN      CNAME           webmail.xinnetvip.com.
smtp                    IN      CNAME           smtp.xinnetvip.com.
pop                     IN      CNAME           pop.xinnetvip.com.

@                       IN      A       61.135.205.147
www                     IN      CNAME   tongyi.ccgslb.net.

ns1                     IN      A       61.135.205.153
ns2                     IN      A       61.135.205.150

cnc.www			IN	A	61.135.205.147
tel.www			IN	A	220.181.40.147

;home                    IN      CNAME   tongyi.ccgslb.net.
home 		10M			IN	A	61.135.205.156
lnzk                    IN      CNAME   lnzk.tongyi.ccgslb.net.
flv                     IN      CNAME   flv.tongyi.com.txcdn.cn.
114 	10M                    IN      A       61.135.205.155
;114                     IN      CNAME    www1.tongyi.ccgslb.net.
;agent                   IN      A       61.135.205.150
;agent                   IN      CNAME    www1.tongyi.ccgslb.net.
agent-test              IN      A       61.135.205.150
agent	10M	IN      A       61.135.205.152
member   10M               IN      CNAME    www1.tongyi.ccgslb.net.
;member 	10M		IN	A	61.135.205.152
;ebook                   IN      CNAME    www1.tongyi.ccgslb.net.
ebook                   IN      A       61.135.205.152
up                      IN      A       61.135.205.152
;site                    IN      CNAME    www1.tongyi.ccgslb.net.
site  	10M                  IN      A    61.135.205.156
;et                      IN      CNAME    www1.tongyi.ccgslb.net.
et    10M                  IN      A	61.135.205.158
info   10M	IN  	    A 	61.135.205.155
;info                    IN      CNAME    www1.tongyi.ccgslb.net.
;message                 IN      CNAME    www1.tongyi.ccgslb.net.
message           10M	IN      A	61.135.205.152
;res                     IN      CNAME    www1.tongyi.ccgslb.net.
res 	10M		IN	A	61.135.205.152
static 	10M		IN	A	61.135.205.152
;static                  IN      CNAME    www1.tongyi.ccgslb.net.
blog                    IN      CNAME    www1.tongyi.ccgslb.net.
www1                    IN      CNAME    www1.tongyi.ccgslb.net.
zhidao                  IN      CNAME    open.zhidao.baidu.com.

;haier                  IN      A       61.135.205.155
haier1                  IN      A       61.135.205.155
haier 10M                  IN      CNAME    haier.tongyi.ccgslb.net.
;haier 	10M                 IN      A       61.135.205.152

anquan                  IN      A       61.135.205.147
answer                  IN      A       61.135.205.155
publish                 IN      A       61.135.205.152

liaoliao                IN      A       61.135.205.152
api                     IN      A       61.135.205.152
wap.api                 IN      A       61.135.205.155
wap                     IN      A       61.135.205.155
wapadmin                IN      A       61.135.205.155
;www1                    IN      A       61.135.205.147
beta    10M                    IN      A       61.135.205.147
2010    10M                    IN      A       61.135.205.147
sxy     10M                    IN      A       61.135.205.147

;member                  IN      A       61.135.205.155
;ebook                   IN      A       61.135.205.151
;et                      IN      A       61.135.205.156
;info                    IN      A       61.135.205.155

;message                 IN      A       61.135.205.153

;res                     IN      A       61.135.205.158
;static                 IN      A       61.135.205.158

s1                      IN      A       60.28.182.176
share                   IN      A       60.28.182.176
;site                    IN      A       61.135.205.153
t                       IN      A       60.28.182.176
;home                    IN      A       61.135.205.153
;blog                    IN      A       61.135.205.153
;www                     IN      CNAME   www.tongyi.chinacache.net.
;mail                    IN      CNAME   freemail-g4.xinnetdns.com.
;lnzk                    IN      A       61.135.205.147
ddzk                    IN      A       61.135.205.147
tlzk                    IN      A       61.135.205.147
dlzk                    IN      A       61.135.205.147
cyzk                    IN      A       61.135.205.147
fxzk                    IN      A       61.135.205.147
pjzk                    IN      A       61.135.205.147
jzzk                    IN      A       61.135.205.147
lyzk                    IN      A       61.135.205.147
ykzk                    IN      A       61.135.205.147
bxzk                    IN      A       61.135.205.147
fszk                    IN      A       61.135.205.147
aszk                    IN      A       61.135.205.147
hldzk                   IN      A       61.135.205.147
wangxianni              IN      A       61.135.205.147
vote                    IN      A       61.135.205.155
voteadmin               IN      A       61.135.205.155
mingshi                 IN      A       61.135.205.155
jinclass                IN      A       61.135.205.155
v                       IN      A       61.135.205.155
video			IN	A	61.135.205.155

; Analyse
analyse			IN	A	61.135.205.159

; feature.tongyi.com
feature			IN	A	61.135.205.147
anglia                  IN      A       61.135.205.155

bbs			IN	A	61.135.205.158

; epress
syepress	10M	IN	A	61.135.205.155

; xxtx.tongyi.com
xxtx		10M	IN	A	61.135.205.155
class		10M	IN	A	61.135.205.152

; om
omtask		10m	IN	A	61.135.205.159
omwiki		10m	IN	A	61.135.205.159
ommon		10m	IN	A	61.135.205.159
omblog		10m	IN	A	61.135.205.159
