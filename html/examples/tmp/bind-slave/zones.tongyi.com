include "/etc/bind/acl.telcom";
include "/etc/bind/acl.edu";

view "view_telcom" {
	match-clients { key slaves-dns2-telcom-tsigkey; 218.61.76.162/32;};
	recursion no;
	allow-transfer { none;};
	server 192.168.2.45 { keys slaves-dns2-telcom-tsigkey; };
	zone "tongyi.com" {
		type slave;
		masters { 192.168.2.45; };
		file "/etc/bind/db.telcom.tongyi.com";
	};

	zone "tonyi.me" {
		type slave;
		masters { 192.168.2.45; };
		file "/etc/bind/db.telcom.tongyi.me";
	};
};

view "view_any" {
	match-clients { any; };
	zone "tongyi.com" { 
		type slave; 
		masters { 192.168.2.45; };
		file "/etc/bind/db.tongyi.com"; };
	zone "tongyi.me" {
		type slave;
		masters { 192.168.2.45; };
		file "/etc/bind/db.tongyi.me"; 
	};
	zone "t1edu.com" { 
		type slave; 
		masters { 192.168.2.45; };
		file "/etc/bind/db.t1edu.com"; };
// prime the server with knowledge of the root servers
zone "." {
	type hint;
	file "/etc/bind/db.root";
};

// be authoritative for the localhost forward and reverse zones, and for
// broadcast zones as per RFC 1912

zone "localhost" {
	type slave;
	masters { 192.168.2.45; };
	file "/etc/bind/db.local";
};

zone "127.in-addr.arpa" {
	type slave;
	masters { 192.168.2.45; };
	file "/etc/bind/db.127";
};

zone "0.in-addr.arpa" {
	type slave;
	masters { 192.168.2.45; };
	file "/etc/bind/db.0";
};

zone "255.in-addr.arpa" {
	type slave;
	masters { 192.168.2.45; };
	file "/etc/bind/db.255";
};


};
