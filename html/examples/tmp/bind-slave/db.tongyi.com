$ORIGIN .
$TTL 3600	; 1 hour
tongyi.com		IN SOA	ns1.tongyi.com. admin.tongyi.com. (
				2011071202 ; serial
				10800      ; refresh (3 hours)
				900        ; retry (15 minutes)
				604800     ; expire (1 week)
				3600       ; minimum (1 hour)
				)
			NS	ns1.tongyi.com.
			NS	ns2.tongyi.com.
			A	220.181.40.147
			MX	0 mx.xinnetvip.com.
$ORIGIN tongyi.com.
$TTL 600	; 10 minutes
114			A	220.181.40.155
2010			A	220.181.40.147
agent			A	220.181.40.152
$TTL 3600	; 1 hour
agent-test		A	220.181.40.150
analyse			A	220.181.40.159
anglia			A	220.181.40.155
anquan			A	220.181.40.147
answer			A	220.181.40.155
api			A	220.181.40.152
$ORIGIN api.tongyi.com.
wap			A	220.181.40.155
$ORIGIN tongyi.com.
aszk			A	220.181.40.147
bbs			A	220.181.40.158
$TTL 600	; 10 minutes
beta			A	220.181.40.147
$TTL 3600	; 1 hour
blog			CNAME	www1.tongyi.ccgslb.net.
bxzk			A	220.181.40.147
$TTL 600	; 10 minutes
class			A	220.181.40.152
$TTL 3600	; 1 hour
cyzk			A	220.181.40.147
ddzk			A	220.181.40.147
dlzk			A	220.181.40.147
ebook			A	220.181.40.152
$TTL 600	; 10 minutes
et			A	220.181.40.158
$TTL 3600	; 1 hour
feature			A	220.181.40.147
flv			CNAME	flv.tongyi.com.txcdn.cn.
fszk			A	220.181.40.147
fxzk			A	220.181.40.147
$TTL 600	; 10 minutes
haier			CNAME	haier.tongyi.ccgslb.net.
$TTL 3600	; 1 hour
haier1			A	220.181.40.155
hldzk			A	220.181.40.147
$TTL 600	; 10 minutes
home			A	220.181.40.156
info			A	220.181.40.155
$TTL 3600	; 1 hour
jinclass		A	220.181.40.155
jzzk			A	220.181.40.147
liaoliao		A	220.181.40.152
lnzk			CNAME	lnzk.tongyi.ccgslb.net.
lyzk			A	220.181.40.147
mail			CNAME	webmail.xinnetvip.com.
mailadmin		CNAME	admin.xinnetvip.com.
$TTL 600	; 10 minutes
member			CNAME	www1.tongyi.ccgslb.net.
message			A	220.181.40.152
$TTL 3600	; 1 hour
mingshi			A	220.181.40.155
ns1			A	220.181.40.153
ns2			A	220.181.40.150
$TTL 600	; 10 minutes
omblog			A	220.181.40.159
ommon			A	220.181.40.159
omtask			A	220.181.40.159
omwiki			A	220.181.40.159
$TTL 3600	; 1 hour
pjzk			A	220.181.40.147
pop			CNAME	pop.xinnetvip.com.
publish			A	220.181.40.152
$TTL 600	; 10 minutes
res			A	220.181.40.152
$TTL 3600	; 1 hour
s1			A	60.28.182.176
share			A	60.28.182.176
$TTL 600	; 10 minutes
site			A	220.181.40.156
$TTL 3600	; 1 hour
smtp			CNAME	smtp.xinnetvip.com.
$TTL 600	; 10 minutes
static			A	220.181.40.152
sxy			A	220.181.40.147
syepress		A	220.181.40.155
$TTL 3600	; 1 hour
t			A	60.28.182.176
tlzk			A	220.181.40.147
up			A	220.181.40.152
v			A	220.181.40.155
video			A	220.181.40.155
vote			A	220.181.40.155
voteadmin		A	220.181.40.155
wangxianni		A	220.181.40.147
wap			A	220.181.40.155
wapadmin		A	220.181.40.155
www			CNAME	tongyi.ccgslb.net.
$ORIGIN www.tongyi.com.
cnc			A	220.181.40.147
tel			A	220.181.40.147
$ORIGIN tongyi.com.
www1			CNAME	www1.tongyi.ccgslb.net.
$TTL 600	; 10 minutes
xxtx			A	220.181.40.155
$TTL 3600	; 1 hour
ykzk			A	220.181.40.147
zhidao			CNAME	open.zhidao.baidu.com.
