$ORIGIN .
$TTL 3600	; 1 hour
t1edu.com		IN SOA	ns1.tongyi.com. admin.tongyi.com. (
				2011051310 ; serial
				10800      ; refresh (3 hours)
				900        ; retry (15 minutes)
				604800     ; expire (1 week)
				3600       ; minimum (1 hour)
				)
$TTL 120	; 2 minutes
			NS	ns1.tongyi.com.
			NS	ns2.tongyi.com.
			A	218.61.76.163
			MX	5 mx1.t1edu.com.
			TXT	"v=spf1 ip4:218.61.76.162 a mx mx:mail.t1edu.com ~all"
$ORIGIN t1edu.com.
imap			A	218.61.76.163
mail			A	218.61.76.163
mx1			A	218.61.76.163
ns1			A	61.135.205.153
ns2			A	61.135.205.150
pop			A	218.61.76.163
smtp			A	218.61.76.163
www			A	218.61.76.163
